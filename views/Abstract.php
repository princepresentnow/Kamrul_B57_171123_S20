<?php
abstract class MyAbstractClass{

    protected  $name;
    abstract public function getData();
    abstract public function setData($value);

    public function displayData(){
        echo "Name: ".$this->name;
    }
}

class MyClass extends MyAbstractClass{
    public function getData()
    {
        return $this->name;
    }

    public function setData($value)
    {
        $this->name = $value;
    }
}

$obj = new MyClass();
$obj->setData("BASIS BITM");
echo $obj->getData();