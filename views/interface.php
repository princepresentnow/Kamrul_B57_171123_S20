<?php
interface CanFly{

    public function canFly();

}

interface CanSwim{

    public function canSwim();
}

class Bird{
    public function birdInfo(){

        echo "I'm a Bird<br>";
        echo "I'm a {{".$this->name."}}<br>";

    }
}

class Penguin extends Bird implements CanSwim{
    protected $name = "Penguin";

    public function canSwim()
    {
        echo "I can Swim<br>";
    }
}

class Dove extends Bird implements CanFly{
    protected $name = "Dove";

    public function canFly()
    {
        echo "I can Fly<br>";
    }
}

class Duck extends Bird implements CanFly,CanSwim{
    protected $name = "Duck";

    public function canFly()
    {
        echo "I can Fly<br>";
    }

    public function canSwim()
    {
        echo "I can Swim<br>";
    }
}

function describe($objBird){

    $objBird->birdInfo();

    if($objBird instanceof CanFly)
   $objBird->canFly();

    if($objBird instanceof CanSwim)
   $objBird->canSwim();
}

describe(new Penguin());
describe(new Dove());
describe(new Duck());