<?php
Trait A{
    public function smallTalk(){
        echo 'a';
    }
    public function mediumTalk(){
        echo 'aA';
    }
    public function bigTalk(){
        echo 'A';
    }

}

Trait B{
    public function smallTalk(){
        echo 'b';
    }
    public function mediumTalk(){
        echo 'bB';
    }
    public function bigTalk(){
        echo 'B';
    }

}

Trait C{
    public function smallTalk(){
        echo 'c';
    }
    public function mediumTalk(){
        echo 'cC';
    }
    public function bigTalk(){
        echo 'C';
    }

}

class MyTraitClass{
    use A,B,C;

}

$obj =new MyTraitClass();
$obj->bigTalk();
